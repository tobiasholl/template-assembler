#include "../template-assembler.h"

using namespace ta::literals;

extern "C" void _start()
{
    // exit_group(42)
    "movl $231, %eax\nmovl $42, %edi\nsyscall"_asm();
}
