#include "../template-assembler.h"
#include "../support/ctll.h"

extern "C" void _start()
{
    // exit_group(42)
    ta::embed<"movl $231, %eax\nmovl $42, %edi\nsyscall">();
}
