#ifndef TA_CTLL_H
#define TA_CTLL_H

#include <ctll/fixed_string.hpp>

namespace ta {
    // Preferred over the auto FixedString variant in the main library.
    // This means you can now use embed<"foo">() instead of embed<"foo"_asm>().
    template <::ctll::fixed_string FixedString>
    [[gnu::always_inline, gnu::flatten]] static inline void embed() {
        ta::emit<FixedString, ta::builtin_identifier>();
        ta::invoke<ta::builtin_identifier>();
    }
} // namespace ta

#endif // TA_CTLL_H
